from random import randint

class Dice():
	"""Class reperesenting a dice"""
	def __init__(self, sides=6):
		self.sides=sides
	def roll(self):
		"""Roll the dice, i.e. return a random variable between 0-sides(default 6)"""
		return(randint(1, self.sides + 1))
