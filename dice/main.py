from dice import Dice
import pygal

ROLL_DICE_NUM=10000

dice=Dice()
results=[]
for num in range(ROLL_DICE_NUM):
	results.append(dice.roll())
#print(results)


#analyze the data
frequency=[]
for num in range(1, dice.sides + 1):
	frequency.append(results.count(num))

print(frequency)

#visualizing the data into a histogram
hist=pygal.Bar()
hist.title='Rolling a dice ' +str(ROLL_DICE_NUM)+ ' times'
hist.x_labels=list(range(1,7))
hist.x_title='Result'
hist.y_title='Freq. of Result'

hist.add('D6', frequency)
hist.render_to_file('dice.svg')