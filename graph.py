import matplotlib.pyplot as plot
from randomwalk import RandomWalk

#plot graph of sq of nos
sq=[]
for num in range(10):
	sq.append(num * num)
plot.plot(sq)
plot.show()
print (sq)

#labeling and increasing thickness of the plot
plot.title('Plot of sq. of natural nos <10', fontsize=14)
plot.xlabel('number', fontsize=10)
plot.ylabel('sq. of number', fontsize=10)
#couldnt understand what this is for
plot.tick_params(axis='both', labelsize=10)
plot.plot(sq, linewidth=5)
plot.show()


#scatter, s=200 defines thickness of the point
plot.scatter(2, 4, s=200)
plot.show()
#plotting multiple points thru scatter
x_val=[]
y_val=[]
for num in range(10):
	x_val.append(num)
	y_val.append(num*num)
plot.scatter(x_val, y_val, s=25)
plot.show()

#same thing just different method to create dataset
x_val=list(range(1001))
y_val=[x*x for x in x_val]
#c to change default color from blue to red. edgecolor='none'to remove black boundary from points
plot.scatter(x_val, y_val, edgecolor='none',c='red', s=10)
plot.axis([0,1000, 0, 1000000])
plot.show()

#color changes as with the values
plot.scatter(x_val, y_val, edgecolor='none',c=y_val, s=10)
plot.axis([0,1000, 0, 1000000])
#saves the plot to dir as png
plot.savefig('plot_of_sq.png', bbox_inches='tight')
plot.show()

randomWalk= RandomWalk()
points=range(randomWalk.num_points)
#plot.scatter(randomWalk.x_val, randomWalk.y_val, edgecolor='none',c=randomWalk.y_val, s=10)
#plot.scatter(randomWalk.x_val, randomWalk.y_val, edgecolor='none',c=randomWalk.y_val, cmap=plot.cm.Blues, s=10)
plot.scatter(randomWalk.x_val, randomWalk.y_val, edgecolor='none',c=points, s=10)
plot.show()

plot.plot(randomWalk.x_val, linewidth=1)
plot.show()