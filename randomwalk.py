from random import choice
class RandomWalk():
	"""Class to generate random walks"""
	def __init__(self, num_points=5000):
		"""Init attributes of the walk"""
		self.num_points=num_points
		self.x_val=[0]
		self.y_val=[0]
		self.fill()

	def fill(self):
		"""Fill the walk"""
		natural_nos=range(1000)
		while len(self.x_val) < self.num_points:
			x_direction=choice([1, -1])
			x_distance=choice(natural_nos)
			x_step= x_direction * x_distance

			y_direction=choice([1, -1])
			y_distance=choice(natural_nos)
			y_step= y_direction * y_distance

			next_x= self.x_val[-1] + x_step
			next_y= self.y_val[-1] + y_step

			self.x_val.append(next_x)
			self.y_val.append(next_y)